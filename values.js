function values(object) {
  if (typeof object == "object") {
    const valuesArray = [];

    for (let key in object) {
      let value = object[key];
      if (typeof value != "function") {
        valuesArray.push(value);
      }
    }

    return valuesArray;
  } else {
    console.log("Data is incorrect");
    return [];
  }
}

module.exports = values;
