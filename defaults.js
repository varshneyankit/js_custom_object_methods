function defaults(object, defaultProps) {
  if (typeof object == "object" && typeof defaultProps == "object") {
    for (let key in defaultProps) {
      if (object[key] == undefined) {
        object[key] = defaultProps[key];
      }
    }
    return object;
  } else {
    console.log("Data is incorrect");
    return null;
  }
}

module.exports = defaults;
