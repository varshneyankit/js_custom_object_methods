function invert(object) {
  if (typeof object == "object") {
    const invertedObject = {};
    for (let key in object) {
      let value = object[key];
      invertedObject[value] = key;
    }
    return invertedObject;
  } else {
    console.log("Data is incorrect");
    return null;
  }
}

module.exports = invert;
