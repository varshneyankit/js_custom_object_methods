function mapObject(object, callback) {
  if (typeof object == "object" && typeof callback == "function") {
    for (let key in object) {
      let value = object[key];
      let changedValue = callback(key, value);
      object[key] = changedValue;
    }
    return object;
  } else {
    console.log("Data is incorrect");
    return null;
  }
}

module.exports = mapObject;
