function keys(object) {
  if (typeof object == "object") {
    const keysArray = [];
    for (let key in object) {
      keysArray.push(String(key));
    }
    return keysArray;
  } else {
    console.log("Data is incorrect");
    return [];
  }
}

module.exports = keys;
