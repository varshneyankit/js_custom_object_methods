const testObject = require("../data");
const mapObject = require("../mapObject");

function callback(key, value) {
  return value + 10;
}

const mappedObject = mapObject(testObject, callback);
console.log(mappedObject);
