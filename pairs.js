function pairs(object) {
  if (typeof object == "object") {
    const pairsArray = [];
    for (let key in object) {
      const pair = [];
      let value = object[key];
      pair.push(key);
      pair.push(value);
      pairsArray.push(pair);
    }
    return pairsArray;
  } else {
    console.log("Data is incorrect");
    return [];
  }
}

module.exports = pairs;
